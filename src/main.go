package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/bwmarrin/discordgo"
	_ "github.com/lib/pq"
	"os"
	"os/signal"
	"syscall"
)

// Global, to save memory.
var psql *sql.DB

const sqlStatement string = `
INSERT INTO users (server, serverid, channel, channelid, user, userid, message, messageid)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`

func main() {
	discord, err := discordgo.New("Bot " + "")
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	discord.AddHandler(messageCreate)

	// Open a websocket connection to Discord and begin listening.
	err = discord.Open()
	if err != nil {
		fmt.Println("[ERR] Error opening connection: ", err.Error())
		return
	}

	connStr := "postgres://logbotdb:taxzYJvtyEqpDUbF33A4WQnHuXGJtQYK@localhost/logbotdb"
	psql, err := sql.Open("postgres", connStr)
	if err != nil {
		fmt.Println("[ERR]: " + err.Error())
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	psql.Close()
	discord.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}

	/*
		INSERT INTO users (server, serverid, channel, channelid, user, userid, message, messageid, mraw)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
	*/
	mraw, err := json.Marshal(m)
	if err != nil {
		fmt.Println("[ERR] ENCODING ERR: " + err.Error())
	}

	guildabs, err := s.Guild(m.GuildID)
	if err != nil {
		fmt.Println("[ERR] DC ERR: " + err.Error())
	}
	chanabs, err := s.Channel(m.ChannelID)
	if err != nil {
		fmt.Println("[ERR] DC ERR: " + err.Error())
	}
	_, err = psql.Exec(sqlStatement, guildabs.Name, m.GuildID, chanabs.Name, m.ChannelID, m.Author.Username, m.Author.ID, m.Content, m.ID, string(mraw))
	if err != nil {
		fmt.Println("[ERR] DB ERR: " + err.Error())
	}

}
